#!/usr/bin/env python3

import sys
import numpy as np
# https://pypi.org/project/pywigxjpf/
import pywigxjpf as wig


# Some SU(2) group theory functions.

def JJ(jphi):
    """ The total isospin """
    return jphi*(jphi+1)

def DD(jphi):
    """ The dimension of the representation """
    return 2*jphi+1

# This one requires the pywigxjpf package, see https://pypi.org/project/pywigxjpf/

def KK(jphi, j1, j2, j3):
    """ The K function from our paper, Eq.(42) """
    max2j = np.amax(np.array([np.amax(int(2*jphi)),\
                                np.amax(2*j1),\
                                np.amax(2*j2),\
                                np.amax(2*j3)]))
    # initialize the Wigner 9j symbol
    wig.wig_table_init(max2j,9)
    wig.wig_temp_init(max2j)

    def KK_scalar(jphi, j1, j2, j3):
        return   DD(j1) * DD(j2)\
               * wig.wig9jj(int(2*j1),   int(2*jphi), int(2*jphi),\
                            int(2*jphi), int(2*j2),   int(2*jphi),\
                            int(2*jphi), int(2*jphi), int(2*j3))


    KK_vec = np.vectorize(KK_scalar, excluded=[0])

    J1 = j1[:,None,None]
    J2 = j2[None,:,None]
    J3 = j3[None,None,:]

    return  KK_vec(jphi, J1, J2, J3)



def beta_g1(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for g1

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    jphi       is the spin of the su2 representation of the scalar field
    Yphi       is the hypercharge of the scalar field

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa
    'ytau'       is the tau Yukawa

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    ng = 3
    if loop == 1:
        # Define the couplings:
        g1 = coupling_dict['g1']
        return   g1**3/(16*np.pi**2)\
                 * (Yphi**2/12 * DD(jphi) + 1/6 + 20/9*ng)
    elif loop == 2:
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        ytau = coupling_dict['ytau']
        return   g1**3/(16*np.pi**2)\
                 * (Yphi**2/12 * DD(jphi) + 1/6 + 20/9*ng)\
               + g1**5/(16*np.pi**2)**2\
                 * (Yphi**4/4 * DD(jphi) + 95/27*ng + 1/2)\
               + g1**3*g2**2/(16*np.pi**2)**2\
                 * (Yphi**2 * DD(jphi) * JJ(jphi) + ng + 3/2)\
               + g1**3*gs**2/(16*np.pi**2)**2 * 44/9*ng\
               - g1**3/(16*np.pi**2)**2\
                 * (17/6*(yt**2 + yc**2) +5/6*yb**2 + 5/2*ytau**2)
    else:
        pass



def beta_g2(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for g2

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    jphi       is the spin of the su2 representation of the scalar field
    Yphi       is the hypercharge of the scalar field

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa
    'ytau'       is the tau Yukawa

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    ng = 3
    if loop == 1:
        # Define the couplings:
        g2 = coupling_dict['g2']
        return   g2**3/(16*np.pi**2)\
                 * (JJ(jphi)*DD(jphi)/9 - 43/6 + 4/3*ng)
    elif loop == 2:
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        ytau = coupling_dict['ytau']
        return   g2**3/(16*np.pi**2)\
                 * (JJ(jphi)*DD(jphi)/9 - 43/6 + 4/3*ng)\
               + g2**5/(16*np.pi**2)**2\
                 * (4/3*JJ(jphi)**2*DD(jphi) + 4/9*JJ(jphi)*DD(jphi)\
                    - 136/3 + 49/3*ng + 13/6)\
               + g1**2*g2**3/(16*np.pi**2)**2\
                 * (Yphi**2/3*DD(jphi)*JJ(jphi) + ng/3 + 1/2)\
               + gs**2*g2**3/(16*np.pi**2)**2\
                 * 4*ng\
               - g2**3/(16*np.pi**2)**2\
                 * (3*(yt**2 + yb**2 + yc**2)/2 + ytau**2/2)
    else:
        pass


def beta_gs(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for gs

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    ng = 3
    if loop == 1:
        # Define the couplings:
        gs = coupling_dict['gs']
        return   gs**3/(16*np.pi**2)\
               * (4/3*ng - 11)
    elif loop == 2:
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        return   gs**3/(16*np.pi**2)\
                 * (4/3*ng - 11)\
               + gs**5/(16*np.pi**2)**2\
                 * (76/3*ng - 102)\
               + g1**2*gs**3/(16*np.pi**2)**2\
                 * 11/18*ng\
               + g2**2*gs**3/(16*np.pi**2)**2\
                 * 3/2*ng\
               - gs**3/(16*np.pi**2)**2\
                 * 2*(yt**2 + yb**2 + yc**2)
    else:
        pass


def beta_yt(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for yt

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    jphi       is the spin of the su2 representation of the scalar field
    Yphi       is the hypercharge of the scalar field

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa
    'ytau'       is the tau Yukawa
    'lamH'       is the SM Higgs self coupling
    'lampH'      is the singlet Higgs portal coupling
    'lampHp'     is the "triplet" Higgs portal coupling
    'lambdaphi'  should be an array of scalar couplings of length jphi+1/2

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    if loop == 1:
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        ytau = coupling_dict['ytau']
        return   yt/(16*np.pi**2)\
                 * ( -17*g1**2/12 -9*g2**2/4 - 8*gs**2\
                     +9*yt**2/2 + 3*yc**2 + 3*yb**2/2 + ytau**2)
    elif loop == 2:
        ng = 3
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        ytau = coupling_dict['ytau']
        lamH = coupling_dict['lamH']
        lampH = coupling_dict['lampH']
        lampHp = coupling_dict['lampHp']
        lambdaphi = coupling_dict['lambdaphi']
        return   yt/(16*np.pi**2)\
                 * ( -17*g1**2/12 -9*g2**2/4 - 8*gs**2\
                     +9*yt**2/2 + 3*yc**2 + 3*yb**2/2 + ytau**2)\
               + yt*g1**4/(16*np.pi**2)**2\
                 * ( 1/8 + 145/81*ng + 5/27*DD(jphi)*Yphi**2)\
               + yt*g2**4/(16*np.pi**2)**2\
                 * ( ng + JJ(jphi)*DD(jphi)/3 - 35/4)\
               - yt*g1**2*g2**2/(16*np.pi**2)**2\
                 * 3/4\
               + yt*gs**4/(16*np.pi**2)**2\
                 * ( 80/9*ng - 404/3)\
               + yt*g1**2/(16*np.pi**2)**2\
                 * ( 131/16*yt**2 + 7/48*yb**2 + 85/24*yc**2 + 25/8*ytau**2)\
               + yt*g1**2*gs**2/(16*np.pi**2)**2\
                 * 19/9\
               + yt*g2**2/(16*np.pi**2)**2\
                 * ( 225/16*yt**2 + 99/16*yb**2 + 45/8*yc**2 + 15/8*ytau**2)\
               + yt*g2**2*gs**2/(16*np.pi**2)**2\
                 * 9\
               + yt*gs**2/(16*np.pi**2)**2\
                 * ( 36*yt**2 + 4*yb**2 + 20*yc**2 )\
               - yt**3/(16*np.pi**2)**2\
                 * ( 12*yt**2 + 11/4*yb**2 + 27/4*yc**2 + 9/4*ytau**2 + 3*lamH)\
               + yt/(16*np.pi**2)**2\
                 * (   DD(jphi)*lampH**2/32\
                     + JJ(jphi)*DD(jphi)*lampHp**2/128
                     + 3/8*lamH**2)\
               - yt/(16*np.pi**2)**2\
                 * ( 1/4*yb**4 + 27/4*yc**4 + 9/4*ytau**4 - 15/4*yb**2*yc**2 - 5/4*yb**2*ytau**2)
    else:
        pass


def beta_yb(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for yb

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    jphi       is the spin of the su2 representation of the scalar field
    Yphi       is the hypercharge of the scalar field

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa
    'ytau'       is the tau Yukawa
    'lamH'       is the SM Higgs self coupling
    'lampH'      is the singlet Higgs portal coupling
    'lampHp'     is the "triplet" Higgs portal coupling
    'lambdaphi'  should be an array of scalar couplings of length jphi+1/2

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    if loop == 1:
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        ytau = coupling_dict['ytau']
        return   yb/(16*np.pi**2)\
                 * ( -5*g1**2/12 -9*g2**2/4 - 8*gs**2\
                     +3*yt**2/2 + 3*yc**2 + 9*yb**2/2 + ytau**2)
    elif loop == 2:
        ng = 3
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        ytau = coupling_dict['ytau']
        lamH = coupling_dict['lamH']
        lampH = coupling_dict['lampH']
        lampHp = coupling_dict['lampHp']
        lambdaphi = coupling_dict['lambdaphi']
        return   yb/(16*np.pi**2)\
                 * ( -5*g1**2/12 -9*g2**2/4 - 8*gs**2\
                     +3*yt**2/2 + 3*yc**2 + 9*yb**2/2 + ytau**2)\
               - yb*g1**4/(16*np.pi**2)**2\
                 * ( 29/72 + 5/81*ng - 7/216*DD(jphi)*Yphi**2)\
               + yb*g2**4/(16*np.pi**2)**2\
                 * ( ng + JJ(jphi)*DD(jphi)/3 - 35/4)\
               - yb*g1**2*g2**2/(16*np.pi**2)**2\
                 * 9/4\
               + yb*gs**4/(16*np.pi**2)**2\
                 * ( 80/9*ng - 404/3)\
               + yb*g1**2/(16*np.pi**2)**2\
                 * ( 91/48*yt**2 + 79/16*yb**2 + 85/24*yc**2 + 25/8*ytau**2)\
               + yb*g1**2*gs**2/(16*np.pi**2)**2\
                 * 31/9\
               + yb*g2**2/(16*np.pi**2)**2\
                 * ( 99/16*yt**2 + 225/16*yb**2 + 45/8*yc**2 + 15/8*ytau**2)\
               + yb*g2**2*gs**2/(16*np.pi**2)**2\
               * 9\
               + yb*gs**2/(16*np.pi**2)**2\
                 * ( 4*yt**2 + 36*yb**2 + 20*yc**2 )\
               - yb**3/(16*np.pi**2)**2\
                 * ( 11/4*yt**2 + 12*yb**2 + 27/4*yc**2 + 9/4*ytau**2 + 3*lamH)\
               + yb/(16*np.pi**2)**2\
                 * (   DD(jphi)*lampH**2/32\
                     + JJ(jphi)*DD(jphi)*lampHp**2/128
                     + 3/8*lamH**2)\
               - yb/(16*np.pi**2)**2\
                 * ( 1/4*yt**4 + 27/4*yc**4 + 9/4*ytau**4 - 15/4*yt**2*yc**2 - 5/4*yt**2*ytau**2)
    else:
        pass


def beta_yc(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for yc

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    jphi       is the spin of the su2 representation of the scalar field
    Yphi       is the hypercharge of the scalar field

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa
    'ytau'       is the tau Yukawa
    'lamH'       is the SM Higgs self coupling
    'lampH'      is the singlet Higgs portal coupling
    'lampHp'     is the "triplet" Higgs portal coupling
    'lambdaphi'  should be an array of scalar couplings of length jphi+1/2

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    if loop == 1:
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        ytau = coupling_dict['ytau']
        return   yc/(16*np.pi**2)\
            * ( -17*g1**2/12 -9*g2**2/4 - 8*gs**2\
                +3*yt**2 + 9*yc**2/2 + 3*yb**2 + ytau**2)
    elif loop == 2:
        ng = 3
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        ytau = coupling_dict['ytau']
        lamH = coupling_dict['lamH']
        lampH = coupling_dict['lampH']
        lampHp = coupling_dict['lampHp']
        lambdaphi = coupling_dict['lambdaphi']
        return   yc/(16*np.pi**2)\
                 * ( -17*g1**2/12 -9*g2**2/4 - 8*gs**2\
                 +3*yt**2 + 9*yc**2/2 + 3*yb**2 + ytau**2)\
               + yc*g1**4/(16*np.pi**2)**2\
                 * ( 1/8 + 145/81*ng + 5/27*DD(jphi)*Yphi**2)\
               + yc*g2**4/(16*np.pi**2)**2\
                 * ( ng + JJ(jphi)*DD(jphi)/3 - 35/4)\
               - yc*g1**2*g2**2/(16*np.pi**2)**2\
                 * 3/4\
               + yc*gs**4/(16*np.pi**2)**2\
                 * ( 80/9*ng - 404/3)\
               + yc*g1**2/(16*np.pi**2)**2\
                 * ( 85/24*yt**2 + 25/24*yb**2 + 131/16*yc**2 + 25/8*ytau**2)\
               + yc*g1**2*gs**2/(16*np.pi**2)**2\
                 * 19/9\
               + yc*g2**2/(16*np.pi**2)**2\
                 * ( 45/8*yt**2 + 45/8*yb**2 + 225/16*yc**2 + 15/8*ytau**2)\
               + yc*g2**2*gs**2/(16*np.pi**2)**2\
                 * 9\
               + yc*gs**2/(16*np.pi**2)**2\
                 * ( 20*yt**2 + 20*yb**2 + 36*yc**2 )\
               - yc**3/(16*np.pi**2)**2\
                 * ( 27/4*yt**2 + 27/4*yb**2 + 12*yc**2 + 9/4*ytau**2 + 3*lamH)\
               + yc/(16*np.pi**2)**2\
                 * (   DD(jphi)*lampH**2/32\
                     + JJ(jphi)*DD(jphi)*lampHp**2/128
                     + 3/8*lamH**2)\
               - yc/(16*np.pi**2)**2\
                 * ( 27/4*yt**4 + 27/4*yb**4 + 9/4*ytau**4 - 3/2*yt**2*yb**2)
    else:
        pass


def beta_ytau(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for ytau

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    jphi       is the spin of the su2 representation of the scalar field
    Yphi       is the hypercharge of the scalar field

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa
    'ytau'       is the tau Yukawa
    'lamH'       is the SM Higgs self coupling
    'lampH'      is the singlet Higgs portal coupling
    'lampHp'     is the "triplet" Higgs portal coupling
    'lambdaphi'  should be an array of scalar couplings of length jphi+1/2

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    if loop == 1:
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        ytau = coupling_dict['ytau']
        return   ytau/(16*np.pi**2)\
            * ( -15*g1**2/4 -9*g2**2/4\
                +3*yt**2 + 3*yc**2 + 3*yb**2 + 5*ytau**2/2)
    elif loop == 2:
        ng = 3
        # Define the couplings:
        g1 = coupling_dict['g1']
        g2 = coupling_dict['g2']
        gs = coupling_dict['gs']
        yt = coupling_dict['yt']
        yb = coupling_dict['yb']
        yc = coupling_dict['yc']
        ytau = coupling_dict['ytau']
        lamH = coupling_dict['lamH']
        lampH = coupling_dict['lampH']
        lampHp = coupling_dict['lampHp']
        lambdaphi = coupling_dict['lambdaphi']
        return   ytau/(16*np.pi**2)\
                 * ( -15*g1**2/4 -9*g2**2/4\
                 +3*yt**2 + 3*yc**2 + 3*yb**2 + 5*ytau**2/2)\
               + ytau*g1**4/(16*np.pi**2)**2\
                 * ( 17/24 + 55/9*ng + 13/24*DD(jphi)*Yphi**2)\
               + ytau*g2**4/(16*np.pi**2)**2\
                 * ( ng + JJ(jphi)*DD(jphi)/3 - 35/4)\
               + ytau*g1**2*g2**2/(16*np.pi**2)**2\
                 * 9/4\
               + ytau*g1**2/(16*np.pi**2)**2\
                 * ( 85/24*yt**2 + 25/24*yb**2 + 85/24*yc**2 + 179/16*ytau**2)\
               + ytau*g2**2/(16*np.pi**2)**2\
                 * ( 45/8*yt**2 + 45/8*yb**2 + 45/8*yc**2 + 165/16*ytau**2)\
               + ytau*gs**2/(16*np.pi**2)**2\
                 * ( 20*yt**2 + 20*yb**2 + 20*yc**2 )\
               - ytau**3/(16*np.pi**2)**2\
                 * ( 27/4*yt**2 + 27/4*yb**2 + 27/4*yc**2 + 3*ytau**2 + 3*lamH)\
               + ytau/(16*np.pi**2)**2\
                 * (   DD(jphi)*lampH**2/32\
                     + JJ(jphi)*DD(jphi)*lampHp**2/128
                     + 3/8*lamH**2)\
               - ytau/(16*np.pi**2)**2\
                 * ( 27/4*yt**4 + 27/4*yb**4 + 27/4*yc**4 - 3/2*yt**2*yb**2)
    else:
        pass


def beta_lambdaphi(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for the scalar quartic couplings (as a numpy array)

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    jphi       is the spin of the su2 representation of the scalar field
    Yphi       is the hypercharge of the scalar field

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa
    'ytau'       is the tau Yukawa
    'lamH'       is the SM Higgs self coupling
    'lampH'      is the singlet Higgs portal coupling
    'lampHp'     is the "triplet" Higgs portal coupling
    'lambdaphi'  should be an array of scalar couplings of length jphi+1/2

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    # Define the couplings:
    g1 = coupling_dict['g1']
    g2 = coupling_dict['g2']
    gs = coupling_dict['gs']
    yt = coupling_dict['yt']
    yb = coupling_dict['yb']
    yc = coupling_dict['yc']
    ytau = coupling_dict['ytau']
    lamH = coupling_dict['lamH']
    lampH = coupling_dict['lampH']
    lambdaphi = coupling_dict['lambdaphi']

    # Make sure that lampHp = 0 for a su2 singlet (jphi = 0):
    if jphi == 0:
        lampHp = 0.
    else:
        lampHp = coupling_dict['lampHp']

    # Check that the correct number of scalar couplings is given:
    assert len(lambdaphi) == np.floor(jphi+1)

    # Generate an array of corresponding primed J values:
    J_prime = (2*(np.arange(np.floor(jphi+1)) - np.floor(jphi) + jphi)).astype(int)

    # Generate an array of full J values:
    J_full = np.arange(2*jphi+1).astype(int)

    # To simplify expression, define the primed sums of the product of two lambdas and K:
    def LLK_primed(J):
        return np.tensordot(lambdaphi,\
                            np.tensordot(lambdaphi,\
                                         KK(jphi, J_prime, J_prime, J),\
                                         axes=[0,0]), axes=[0,0])
    def L2LK_primed(J):
        return np.tensordot(lambdaphi**2,\
                            np.tensordot(lambdaphi,\
                                         KK(jphi, J_prime, J_prime, J),\
                                         axes=[0,0]), axes=[0,0])
    # Define the symmetry phase factor:
    def phase(J):
        return (-1)**abs(J-2*jphi)

    # Number of SM fermion generations:
    ng = 3

    Bphi1l = (lambdaphi/(4*np.pi))**2\
           + 3*g2**2/(8*np.pi**2)\
             * (g2**2 * (  (JJ(J_prime) - 2*JJ(jphi))**2\
                         + (JJ(J_prime) - 2*JJ(jphi)))\
             - 2*lambdaphi*JJ(jphi))\
           + 1/(4*np.pi**2) * LLK_primed(J_prime)\
           + 3*g1**4/(32*np.pi**2) * Yphi**4\
           - 3*g1**2/(16*np.pi**2) * Yphi**2 * lambdaphi\
           + 3*g1**2*g2**2/(8*np.pi**2) * Yphi**2 * (JJ(J_prime) - 2*JJ(jphi))\
           + lampH**2/(32*np.pi**2)\
           + lampHp**2/(256*np.pi**2) * (JJ(J_prime) - 2*JJ(jphi))

    if loop == 1:
        return Bphi1l

    elif loop == 2:
        Bphi60 = - Yphi**6/4 * (7/3*DD(jphi) + 15) * np.ones(np.floor(jphi+1).astype(int))\
                 - Yphi**4 * (7/6 + 80/9*ng) * np.ones(np.floor(jphi+1).astype(int))
        Bphi42 =   Yphi**4 * (  JJ(jphi) * (7/3*DD(jphi) + 15)\
                              - JJ(J_prime) * (7/6*DD(jphi) + 15))\
                 - Yphi**2 * (7/3 + 160/9*ng) * (JJ(J_prime) - 2*JJ(jphi))
        Bphi24 = Yphi**2 * (  JJ(jphi) * (60*JJ(jphi) + 28/9*DD(jphi)*JJ(jphi) - 218/3 + 64/3*ng)\
                            - JJ(J_prime) * (15*JJ(J_prime) + 14/9*DD(jphi)*JJ(jphi) - 109/3 + 32/3*ng))
        Bphi06 = (JJ(J_prime)/2 - JJ(jphi)) * (JJ(J_prime)/2 - JJ(jphi) + 1/2)\
                 * (584/3 - 112/9*DD(jphi)*JJ(jphi) - 240*JJ(jphi) - 256/3*ng)\
                 + 216 * JJ(jphi)**2
        Bphi20 =   Yphi**2 * (8 * LLK_primed(J_prime) - lambdaphi**2)\
                 + 1/8*lampHp**2 * (JJ(J_prime) - 2*JJ(jphi)) + lampH**2
        Bphi02 =   lambdaphi**2 * (8*JJ(jphi) - 3*JJ(J_prime))\
                 - 16 * JJ(jphi) * LLK_primed(J_prime)\
                 + 12 * np.tensordot(JJ(J_full)*phase(J_full),\
                                     np.tensordot(LLK_primed(J_full),\
                                                  KK(jphi, J_full, J_full, J_prime),\
                                                  axes=[0,0]), axes=[0,0])\
                 + 3 * lampH**2
        Bphi40 =   Yphi**4 * (  (9/8 + 11/24*DD(jphi))*lambdaphi\
                         + 5*np.dot(lambdaphi, DD(J_prime))/DD(jphi))\
                 + Yphi**2 * (lambdaphi * (11/12 + 50/9*ng) + 5/2*lampH)
        if jphi == 0:
            Bphi22 = 0.
        else:
            Bphi22 =   Yphi**2 * (  10*np.dot(lambdaphi,\
                                               DD(J_prime)*JJ(J_prime))/DD(jphi)/JJ(jphi)*JJ(J_prime)\
                                    - 20*np.dot(lambdaphi,\
                                               DD(J_prime)*JJ(J_prime))/DD(jphi)\
                                    - 20*np.dot(lambdaphi,\
                                               DD(J_prime))/DD(jphi)*JJ(J_prime)\
                                    + 40*np.dot(lambdaphi,\
                                                DD(J_prime))/DD(jphi)*JJ(jphi))\
                     + Yphi**2 * lambdaphi * (JJ(jphi) + 2*JJ(J_prime))\
                     + 5/2*Yphi*lampHp * (JJ(J_prime) - 2*JJ(jphi))
        if jphi == 0:
            Bphi04 = 0.
        else:
            Bphi04 =   lambdaphi * (  JJ(jphi) * (18*JJ(jphi) + 22/9*DD(jphi)*JJ(jphi) - 275/3 + 40/3*ng)\
                                      + JJ(J_prime) * (JJ(J_prime) - 4*JJ(jphi) + 2))\
                     + 4*np.tensordot(np.tensordot(lambdaphi,\
                                                   KK(jphi, J_prime, J_prime, J_prime),\
                                                   axes=[0,1]),\
                                      (JJ(J_prime)**2 - 4*JJ(J_prime)*JJ(jphi)),\
                                      axes=[0,0])\
                     + np.tensordot(np.tensordot(lambdaphi,\
                                                 KK(jphi, J_full, J_prime, J_prime),\
                                                 axes=[0,1]),\
                                    (18*JJ(J_full)**2 - 72*JJ(J_full)*JJ(jphi)),\
                                    axes=[0,0])\
                     + 80  * np.dot(lambdaphi, DD(J_prime))*JJ(jphi)**2/DD(jphi)\
                     + 40  * np.dot(lambdaphi, DD(J_prime))*JJ(jphi)/DD(jphi)\
                     - 20  * np.dot(lambdaphi, DD(J_prime)*JJ(J_prime))/DD(jphi)\
                     - 20  * np.dot(lambdaphi, DD(J_prime))*JJ(J_prime)/DD(jphi)\
                     + 10  * np.dot(lambdaphi, DD(J_prime)*JJ(J_prime))*JJ(J_prime)/JJ(jphi)/DD(jphi)\
                     + 10 * lampH * JJ(jphi)
        Bphi00 =   lambdaphi * np.dot(lambdaphi**2, DD(J_prime))/(DD(jphi))\
                 - 4*LLK_primed(J_prime) * lambdaphi\
                 - 4*L2LK_primed(J_prime)\
                 - 8 * np.tensordot(lambdaphi,\
                                    np.tensordot(LLK_primed(J_full)*phase(J_full),\
                                                 KK(jphi, J_full, J_prime, J_prime),\
                                                 axes=[0,0]), axes=[0,0])\
                 - lambdaphi*lampHp**2/16 * (2*JJ(J_prime) - 3*JJ(jphi))\
                 - lampH*lampHp**2/8 * (JJ(J_prime) - JJ(jphi))\
                 - lampHp**2/8 * (3*yt**2 + 3*yc**2 + 3*yb**2 + ytau**2) * (JJ(J_prime) - 2*JJ(jphi))\
                 - lampH**2 * (3*yt**2 + 3*yc**2 + 3*yb**2 + ytau**2 + 5/4*lambdaphi)\
                 - lampH**3/2
        return ( g1**6         * Bphi60\
               + g1**4 * g2**2 * Bphi42\
               + g1**2 * g2**4 * Bphi24\
               +         g2**6 * Bphi06\
               + g1**4         * Bphi40\
               + g1**2 * g2**2 * Bphi22\
               +         g2**4 * Bphi04\
               + g1**2         * Bphi20\
               +         g2**2 * Bphi02\
               +                 Bphi00)/(16*np.pi**2)**2 + Bphi1l
    else:
        pass


def beta_lamH(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for the Higgs quartic coupling

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    jphi       is the spin of the su2 representation of the scalar field
    Yphi       is the hypercharge of the scalar field

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa
    'ytau'       is the tau Yukawa
    'lamH'       is the SM Higgs self coupling
    'lampH'      is the singlet Higgs portal coupling
    'lampHp'     is the "triplet" Higgs portal coupling

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    # Define the couplings:
    g1 = coupling_dict['g1']
    g2 = coupling_dict['g2']
    gs = coupling_dict['gs']
    yt = coupling_dict['yt']
    yb = coupling_dict['yb']
    yc = coupling_dict['yc']
    ytau = coupling_dict['ytau']
    lamH = coupling_dict['lamH']
    lampH = coupling_dict['lampH']

    # Make sure that lampHp = 0 for a su2 singlet (jphi = 0):
    if jphi == 0:
        lampHp = 0.
    else:
        lampHp = coupling_dict['lampHp']

    BH1l =   3*lamH**2/(8*np.pi**2)\
           + g1**2/(16*np.pi**2) * (3*g1**2/2 - 3*lamH)\
           + g2**2/(16*np.pi**2) * (9*g2**2/2 - 9*lamH)\
           + 3*g1**2*g2**2/(16*np.pi**2)\
           - (3*(yt**4 + yc**4 + yb**4) + ytau**4)/(2*np.pi**2)\
           + lamH * (3*(yt**2 + yc**2 + yb**2) + ytau**2)/(4*np.pi**2)\
           + lampH**2/(64*np.pi**2) * DD(jphi)\
           + lampHp**2/(768*np.pi**2) * DD(jphi) * JJ(jphi)

    if loop == 1:
        return BH1l

    elif loop == 2:
        ng = 3
        BH60 = - Yphi**2 * 7/12*DD(jphi) - (59/12 + 80/9*ng)
        BH42 = - Yphi**2 * 7/12*DD(jphi) - (239/12 + 80/9*ng)
        BH24 = - 7/9 * JJ(jphi) * DD(jphi) - 97/12 - 16/3*ng
        BH06 = - 7/3 * JJ(jphi) * DD(jphi) + 497/4 - 16*ng
        BH40 =   1/24*lamH * (11*Yphi**2*DD(jphi) + 229)\
                + 5/4*lampH * Yphi**2*DD(jphi)\
                + 50/9 * ng * lamH\
                - (19*yt**2 - 5*yb**2 + 19*yc**2 + 25*ytau**2)
        BH22 =   39/4*lamH + 5/6*lampHp*Yphi*JJ(jphi)*DD(jphi)\
               + 2*(21*yt**2 + 9*yb**2 + 21*yc**2 + 11*ytau**2)
        BH04 =   5*lampH*JJ(jphi)*DD(jphi)\
               - 3*(3*yt**2 + 3*yb**2 + 3*yc**2 + ytau**2)\
               + lamH * (10*ng - 313/8 + 11/6*JJ(jphi)*DD(jphi))
        BH20 =   9*lamH**2 + 1/2*lampH**2*Yphi**2*DD(jphi)\
               + 1/24*lampHp**2*Yphi**2*JJ(jphi)*DD(jphi)\
               + lamH/6*(85*yt**2 + 25*yb**2 + 85*yc**2 + 75*ytau**2)\
               - 4/3 * (8*yt**4 - 4*yb**4 + 8*yc**4 + 12*ytau**4)
        BH02 =   27*lamH**2 + 2*lampH**2*JJ(jphi)*DD(jphi)\
               + lampHp**2*JJ(jphi)*DD(jphi) * (JJ(jphi)/6 - 1/8)\
               + 15/2*lamH*(3*yt**2 + 3*yb**2 + 3*yc**2 + ytau**2)
        BH00 = - 39/2*lamH**3 - DD(jphi)/4*lampH**3\
               - 5/8*lampH**2*lamH*DD(jphi)\
               - 7/96*lampHp**2*lamH*JJ(jphi)*DD(jphi)\
               - 5/48*lampHp**2*lampH*JJ(jphi)*DD(jphi)\
               - 12*lamH**2*(3*yt**2 + 3*yb**2 + 3*yc**2 + ytau**2)\
               + 80*lamH*gs**2*(yt**2 + yb**2 + yc**2)\
               - lamH*(3*yt**4 + 3*yb**4 + 3*yc**4 + ytau**4 + 42*yt**2*yb**2)\
               + 120*(yt**6 + yb**6 + yc**6) + 40*ytau**6\
               - 24*(yt**2*yb**4 + yt**4*yb**2)\
               - 128*gs**2*(yt**4 + yb**4 + yc**4)

        return ( g1**6         * BH60\
               + g1**4 * g2**2 * BH42\
               + g1**2 * g2**4 * BH24\
               +         g2**6 * BH06\
               + g1**4         * BH40\
               + g1**2 * g2**2 * BH22\
               +         g2**4 * BH04\
               + g1**2         * BH20\
               +         g2**2 * BH02\
               +                 BH00)/(16*np.pi**2)**2 + BH1l
    else:
        pass


def beta_lampH(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for the "singlet" Higgs-portal coupling

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    jphi       is the spin of the su2 representation of the scalar field
    Yphi       is the hypercharge of the scalar field

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa
    'ytau'       is the tau Yukawa
    'lamH'       is the SM Higgs self coupling
    'lampH'      is the singlet Higgs portal coupling
    'lampHp'     is the "triplet" Higgs portal coupling
    'lambdaphi'  should be an array of scalar couplings of length jphi+1/2

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    # Define the couplings:
    g1 = coupling_dict['g1']
    g2 = coupling_dict['g2']
    gs = coupling_dict['gs']
    yt = coupling_dict['yt']
    yb = coupling_dict['yb']
    yc = coupling_dict['yc']
    ytau = coupling_dict['ytau']
    lamH = coupling_dict['lamH']
    lampH = coupling_dict['lampH']
    lambdaphi = coupling_dict['lambdaphi']

    # Make sure that lampHp = 0 for a su2 singlet (jphi = 0):
    if jphi == 0:
        lampHp = 0.
    else:
        lampHp = coupling_dict['lampHp']

    # Check that the correct number of scalar couplings is given:
    assert len(lambdaphi) == np.floor(jphi+1)

    # Generate an array of corresponding primed J values:
    J_prime = (2*(np.arange(np.floor(jphi+1)) - np.floor(jphi) + jphi)).astype(int)

    BpH1l =  lampHp**2/(64*np.pi**2)*JJ(jphi)\
           + g1**2/(16*np.pi**2) * (3*g1**2*Yphi**2 - 3*lampH*(1+Yphi**2)/2)\
           + g2**2/(16*np.pi**2) * (12*g2**2*JJ(jphi) - lampH*(9/2+6*JJ(jphi)))\
           + lampH * (3*(yt**2 + yc**2 + yb**2) + ytau**2)/(8*np.pi**2)\
           + lampH/(8*np.pi**2) * np.dot(lambdaphi,DD(J_prime))/DD(jphi)\
           + lampH**2/(16*np.pi**2)\
           + 3*lampH*lamH/(16*np.pi**2)

    if loop == 1:
        return BpH1l

    elif loop == 2:
        ng = 3
        BpH60 = - Yphi**4 * (7/6*DD(jphi) + 15/4) - Yphi**2 * (73/12 + 160/9*ng)
        BpH42 = - Yphi**2 * (15*JJ(jphi) + 45/4)
        BpH24 = - 15*JJ(jphi) * (1 + Yphi**2)
        BpH06 = JJ(jphi) * (1129/3 - 128/3*ng - 60*JJ(jphi) - 56/9*JJ(jphi)*DD(jphi))
        BpH40 =   Yphi**2 * (   15/2*lamH - (19*yt**2 - 5*yb**2 + 19*yc**2 + 25*ytau**2)\
                              + 5*np.dot(lambdaphi, DD(J_prime))/DD(jphi))\
                + Yphi**2 * lampH * (23/24 + 11/48*DD(jphi))\
                + Yphi**4 * lampH * (5/16 + 71/48*DD(jphi))\
                + lampH * (157/48 + 25/9*ng * (1 + Yphi**2))
        BpH22 = Yphi*JJ(jphi)*lampHp + lampH * (15/8 + 5/2*JJ(jphi)*Yphi**2)
        BpH04 =   JJ(jphi) * (   30*lamH - 4*(3*(yt**2 + yb**2 + yc**2) + ytau**2)\
                               + 20*np.dot(lambdaphi, DD(J_prime))/DD(jphi))\
                - lampH * (   385/16 - 5*ng\
                            + JJ(jphi) * (263/6 - 20/3*ng - 11/12*DD(jphi))\
                            - JJ(jphi)**2 * (5 + 71/9*DD(jphi)))
        BpH20 =   lampHp**2/16 * JJ(jphi) * (1 + Yphi**2)\
                + lampH**2/4  * (1 + Yphi**2)\
                + lampH/12 * (85*yt**2 + 25*yb**2 + 85*yc**2 + 75*ytau**2)\
                + 6*lampH*lamH\
                + 4*Yphi**2*lampH * np.dot(lambdaphi, DD(J_prime))/DD(jphi)
        BpH02 =   lampHp**2/16 * JJ(jphi) * (15 + 4*JJ(jphi))\
                + lampH**2/4  * (3 + 4*JJ(jphi))\
                + 15/4*lampH * (3*yt**2 + 3*yb**2 + 3*yc**2 + ytau**2)\
                + 18*lampH*lamH\
                + 16*JJ(jphi)*lampH * np.dot(lambdaphi, DD(J_prime))/DD(jphi)
        BpH00 = - (lampHp**2/4*JJ(jphi) + 6*lampH*lamH + lampH**2)\
                  *(3*yt**2 + 3*yb**2 + 3*yc**2 + ytau**2)\
                + 40*lampH*gs**2 * (yt**2 + yb**2 + yc**2)\
                - lampH/2 * (27*yt**4 + 27*yb**4 + 27*yc**4 + 9*ytau**4 + 42*yt**2*yb**2)\
                - 5/8*JJ(jphi)*lampHp**2*lamH\
                - 15/4*lampH*lamH**2\
                - lampH*lampHp**2 * (13/32 + DD(jphi)/64) * JJ(jphi)\
                - 9/2*lampH**2*lamH\
                - lampH**3 * (5/8 + DD(jphi)/16)\
                - 5/2*lampH * np.dot(lambdaphi*lambdaphi, DD(J_prime))/DD(jphi)\
                - lampHp**2/4 * np.dot(lambdaphi, DD(J_prime)*(JJ(J_prime)-JJ(jphi)))/DD(jphi)\
                - 3*lampH**2 * np.dot(lambdaphi, DD(J_prime))/DD(jphi)

        return ( g1**6         * BpH60\
               + g1**4 * g2**2 * BpH42\
               + g1**2 * g2**4 * BpH24\
               +         g2**6 * BpH06\
               + g1**4         * BpH40\
               + g1**2 * g2**2 * BpH22\
               +         g2**4 * BpH04\
               + g1**2         * BpH20\
               +         g2**2 * BpH02\
               +                 BpH00)/(16*np.pi**2)**2 + BpH1l
    else:
        pass


def beta_lampHp(jphi, Yphi, coupling_dict, loop):
    """ Return the beta function for the "triplet" Higgs-portal coupling

    Note that this beta function does not appear for an electroweak singlet (jphi=0).

    See arxiv:2007.13755 [hep-ph] for conventions
    regarding normalization of couplings and hypercharge, and the beta function

    jphi       is the spin of the su2 representation of the scalar field
    Yphi       is the hypercharge of the scalar field

    coupling_dict is a python dictionary that should contain values
    for the following keys, corresponding to the values of the couplings:

    'g1'         is the U(1) coupling
    'g2'         is the SU(2) coupling
    'gs'         is the SU(3) coupling
    'yt'         is the top Yukawa
    'yb'         is the bottom Yukawa
    'yc'         is the charm Yukawa
    'ytau'       is the tau Yukawa
    'lamH'       is the SM Higgs self coupling
    'lampH'      is the singlet Higgs portal coupling
    'lampHp'     is the "triplet" Higgs portal coupling
    'lambdaphi'  should be an array of scalar couplings of length jphi+1/2

    All other Yukawa couplings are neglected (set to zero)

    loop is the loop order (1 or 2)
    """

    # Define the couplings:
    g1 = coupling_dict['g1']
    g2 = coupling_dict['g2']
    gs = coupling_dict['gs']
    yt = coupling_dict['yt']
    yb = coupling_dict['yb']
    yc = coupling_dict['yc']
    ytau = coupling_dict['ytau']
    lamH = coupling_dict['lamH']
    lampH = coupling_dict['lampH']
    lampHp = coupling_dict['lampHp']
    lambdaphi = coupling_dict['lambdaphi']

    # Check that the correct number of scalar couplings is given:
    assert len(lambdaphi) == np.floor(jphi+1)

    # Check that jphi != 0:
    if jphi == 0:
        raise Exception("This beta function does not exist for jphi = 0.")

    # Generate an array of corresponding primed J values:
    J_prime = (2*(np.arange(np.floor(jphi+1)) - np.floor(jphi) + jphi)).astype(int)

    BpHp1l = lampHp/(16*np.pi**2)/JJ(jphi)/DD(jphi)\
             * np.dot(lambdaphi, DD(J_prime)*(JJ(J_prime)-2*JJ(jphi)))\
           - 3*g1**2/(32*np.pi**2) * lampHp * (1+Yphi**2)\
           - g2**2/(16*np.pi**2) * lampHp*(9/2+6*JJ(jphi))\
           + 3*g1**2*g2**2/(2*np.pi**2) * Yphi\
           + lampHp * (3*(yt**2 + yc**2 + yb**2) + ytau**2)/(8*np.pi**2)\
           + lampHp*lamH/(16*np.pi**2)\
           + lampHp*lampH/(8*np.pi**2)

    if loop == 1:
        return BpHp1l

    elif loop == 2:
        ng = 3
        BpHp60 = 0
        BpHp42 = - Yphi**3 * (14/3*DD(jphi) + 30) - Yphi * (118/3 + 640/9*ng)
        BpHp24 = Yphi * (346/3 - 128/3*ng -120*JJ(jphi) - 56/9*DD(jphi)*JJ(jphi))
        BpHp06 = 0
        BpHp40 =   11/48*Yphi**2*(1+Yphi**2)*lampHp*DD(jphi)\
                 + 5/16*Yphi**4*lampHp\
                 + lampHp*(37/48 + 23/24*Yphi**2 + 25/9*ng*(1+Yphi**2))
        BpHp22 =   lampHp*(47/8 + Yphi**2*(10/3*DD(jphi)*JJ(jphi) + 5/2*JJ(jphi) - 1))\
                 + 4*Yphi*(lampH + 5*lamH)\
                 + 4*Yphi*(42*yt**2 + 18*yb**2 + 42*yc**2 + 22*ytau**2)\
                 + 20*Yphi/DD(jphi)/JJ(jphi)\
                   *np.dot(lambdaphi*(JJ(J_prime)-2*JJ(jphi)),DD(J_prime))
        BpHp04 =   (5*ng - 457/16\
                 + JJ(jphi)*(20/3*ng - 287/6 + 11/12*DD(jphi) + JJ(jphi)*(5 + 11/9*DD(jphi))))*lampHp
        BpHp20 =   3/4*Yphi*lampHp**2 + 1/2*lampH*lampHp*(1+Yphi**2)\
                 + lampHp/12 * (85*yt**2 + 25*yb**2 + 85*yc**2 + 75*ytau**2)\
                 + 2*lampHp*lamH\
                 + 2*Yphi**2*lampHp/DD(jphi)/JJ(jphi)\
                   *np.dot(lambdaphi*(JJ(J_prime)-2*JJ(jphi)),DD(J_prime))
        BpHp02 =   15/4*lampHp * (3*yt**2 + 3*yb**2 + 3*yc**2 + ytau**2)\
                 + lampHp*lampH*(15/2 + 2*JJ(jphi))\
                 + (8*JJ(jphi) - 6)*lampHp/DD(jphi)/JJ(jphi)\
                   *np.dot(lambdaphi*(JJ(J_prime)-2*JJ(jphi)),DD(J_prime))
        BpHp00 = - (lampHp*lamH + lampH*lampHp)\
                   *2*(3*yt**2 + 3*yb**2 + 3*yc**2 + ytau**2)\
                 + 40*lampHp*gs**2 * (yt**2 + yb**2 + yc**2)\
                 - lampHp/2 * (27*yt**4 + 27*yb**4 + 27*yc**4 + 9*ytau**4 - 54*yt**2*yb**2)\
                 - 5*lampH*lampHp*lamH\
                 - 7/4*lampHp*lamH**2\
                 - lampHp*lampH**2 * (13/8 + DD(jphi)/16)\
                 + lampHp**3 * (3/16 - 5*JJ(jphi)/32 + 5*JJ(jphi)*DD(jphi)/192)\
                 - lampHp/2*np.dot(lambdaphi**2, (2*JJ(J_prime)*DD(J_prime)/JJ(jphi)/DD(jphi)\
                                                  -3*DD(J_prime)/DD(jphi)))\
                 - 2*lampH*lampHp*np.dot(lambdaphi, (JJ(J_prime)*DD(J_prime)/JJ(jphi)/DD(jphi)\
                                                     -DD(J_prime)/DD(jphi)))

        return ( g1**6         * BpHp60\
               + g1**4 * g2**2 * BpHp42\
               + g1**2 * g2**4 * BpHp24\
               +         g2**6 * BpHp06\
               + g1**4         * BpHp40\
               + g1**2 * g2**2 * BpHp22\
               +         g2**4 * BpHp04\
               + g1**2         * BpHp20\
               +         g2**2 * BpHp02\
               +                 BpHp00)/(16*np.pi**2)**2 + BpHp1l
    else:
        pass

