#!/usr/bin/env python3


import sys
import numpy as np
import scipy as sp
from scipy.integrate import ode
from scipy.integrate import odeint
from scipy.interpolate import interp1d
sys.path.append('.')

import complex_multiplet_beta_function as cmb

# Dictionaries for j=0 and j=3, with SM couplings defined at mu=MZ (see arxiv:2007.13755 for details):

coupj0 = {'g1': 0.3574189131413059,\
          'g2': 0.6517455668900911,\
          'gs': 1.1626307142857144,\
          'yt': 0.9319823571428572,\
          'yb': 0.01643925754280227,\
          'yc': 0.003613176180263253,\
          'ytau': 0.010205763440624986,\
          'lamH': 0.5040199999999999,\
          'lampH': 0.5,\
          'lampHp': 0.5,\
          'lambdaphi': np.array([0.5])}

coupj3  = {'g1': 0.3574189131413059,\
           'g2': 0.6517455668900911,\
           'gs': 1.1626307142857144,\
           'yt': 0.9319823571428572,\
           'yb': 0.01643925754280227,\
           'yc': 0.003613176180263253,\
           'ytau': 0.010205763440624986,\
           'lamH': 0.5040199999999999,\
           'lampH': 0.5,\
           'lampHp': 0.5,\
           'lambdaphi': np.array([0.5, 0.5, 0.5, 0.5])}

# Calculate a few beta functions at two loops:

print("Two-loop beta function for g2, with jphi=3 and Yphi=1:")
print("beta_g2 = ", cmb.beta_g2(3,1,coupj3,2))
print("\n")

print("Two-loop beta function for lambdaphi, with jphi=3 and Yphi=1:")
print("beta_lambdaphi = ", cmb.beta_lambdaphi(3,1,coupj3,2))
print("\n")

print("Two-loop beta function for lambdaphi, with jphi=0 and Yphi=0:")
print("beta_lambdaphi = ", cmb.beta_lambdaphi(0,0,coupj0,2))
print("\n")

# Uncomment the following lines if you want to see some built-in error messages:
# print("Specifying a coupling array with wrong length results in an error:")
# print("beta_lambdaphi = ", cmb.beta_lambdaphi(3,1,coupj0,2))
# print("\n")
# print("The beta function for the triplet Higgs-portal coupling does not exist for a weak singlet:")
# print("beta_lambdaphi = ", cmb.beta_lampHp(0,1,coupj0,2))



# Here is a simple routine to calculate the running couplings:
# (Use with care -- this is just for illustration
#  and should not necessarily be used for actual numerics!)


class couplings(object):
    def __init__(self, jphi, Yphi, coup_init, initial_mu):
        """ Calculate the running of the gauge and scalar couplings in the unbroken EW theory

        jphi       is the spin of the su2 representation of the scalar field
        Yphi       is the hypercharge of the scalar field, 
                   such that Q = I3 + Yphi/2 (Denner convention)
        coup_init  is a dictionary with the initial conditions of the couplings
                   specified at scale initial_mu
        initial_mu is the starting scale of the RG evolution
        """

        self.jphi = jphi
        self.Yphi = Yphi
        self.g1 = coup_init['g1']
        self.g2 = coup_init['g2']
        self.gs = coup_init['gs']
        self.yt = coup_init['yt']
        self.yb = coup_init['yb']
        self.yc = coup_init['yc']
        self.ytau = coup_init['ytau']
        self.lamH = coup_init['lamH']
        self.lampH = coup_init['lampH']
        self.lampHp = coup_init['lampHp']
        self.lambdaphi = coup_init['lambdaphi']
        self.initial_mu = initial_mu

        # The initial values of the couplings at mu = initial_mu
        self.ginit = np.hstack((np.array(self.g1),\
                                np.array(self.g2),\
                                np.array(self.gs),\
                                np.array(self.yt),\
                                np.array(self.yb),\
                                np.array(self.yc),\
                                np.array(self.ytau),\
                                np.array(self.lamH),\
                                np.array(self.lampH),\
                                np.array(self.lampHp),\
                                self.lambdaphi))


    def _dgdmu(self, coup_array, loop, mu):
        """ Return the derivative [i.e. dg/dmu == beta/mu] of the couplings.

        Here, coup_array is an array of the couplings in the order
        g1, g2, gs, yt, yb, yc, ytau, lamH, lampH, lampHp, lambdaphi
        """

        my_dict = {}
        my_dict['g1'] = coup_array[0]
        my_dict['g2'] = coup_array[1]
        my_dict['gs'] = coup_array[2]
        my_dict['yt'] = coup_array[3]
        my_dict['yb'] = coup_array[4]
        my_dict['yc'] = coup_array[5]
        my_dict['ytau'] = coup_array[6]
        my_dict['lamH'] = coup_array[7]
        my_dict['lampH'] = coup_array[8]
        my_dict['lampHp'] = coup_array[9]
        my_dict['lambdaphi'] = np.array(coup_array[10:])

        if loop == 1:
            lam_array = cmb.beta_lambdaphi(self.jphi, self.Yphi, my_dict, 1)
            g1_array  = np.array(cmb.beta_g1(self.jphi, self.Yphi, my_dict, 1))
            g2_array  = np.array(cmb.beta_g2(self.jphi, self.Yphi, my_dict, 1))
            gs_array  = np.array(cmb.beta_gs(self.jphi, self.Yphi, my_dict, 1))
            yt_array  = np.array(cmb.beta_yt(self.jphi, self.Yphi, my_dict, 1))
            yb_array  = np.array(cmb.beta_yb(self.jphi, self.Yphi, my_dict, 1))
            yc_array  = np.array(cmb.beta_yc(self.jphi, self.Yphi, my_dict, 1))
            ytau_array  = np.array(cmb.beta_ytau(self.jphi, self.Yphi, my_dict, 1))
            lamH_array  = np.array(cmb.beta_lamH(self.jphi, self.Yphi, my_dict, 1))
            lampH_array  = np.array(cmb.beta_lampH(self.jphi, self.Yphi, my_dict, 1))
            lampHp_array  = np.array(cmb.beta_lampHp(self.jphi, self.Yphi, my_dict, 1))
        elif loop == 2:
            lam_array = cmb.beta_lambdaphi(self.jphi, self.Yphi, my_dict, 2)
            g1_array  = np.array(cmb.beta_g1(self.jphi, self.Yphi, my_dict, 2))
            g2_array  = np.array(cmb.beta_g2(self.jphi, self.Yphi, my_dict, 2))
            gs_array  = np.array(cmb.beta_gs(self.jphi, self.Yphi, my_dict, 2))
            yt_array  = np.array(cmb.beta_yt(self.jphi, self.Yphi, my_dict, 2))
            yb_array  = np.array(cmb.beta_yb(self.jphi, self.Yphi, my_dict, 2))
            yc_array  = np.array(cmb.beta_yc(self.jphi, self.Yphi, my_dict, 2))
            ytau_array  = np.array(cmb.beta_ytau(self.jphi, self.Yphi, my_dict, 2))
            lamH_array  = np.array(cmb.beta_lamH(self.jphi, self.Yphi, my_dict, 2))
            lampH_array  = np.array(cmb.beta_lampH(self.jphi, self.Yphi, my_dict, 2))
            lampHp_array  = np.array(cmb.beta_lampHp(self.jphi, self.Yphi, my_dict, 2))
        else:
            pass

        return np.hstack((g1_array, g2_array, gs_array, yt_array, yb_array,\
                          yc_array, ytau_array, lamH_array, lampH_array,\
                          lampHp_array, lam_array)) / mu


    def run(self, loop, mu_final):
        """ Calculate the running of the couplings from 
            initial_mu to mu_final. """

        # Runge-Kutta:
        def deriv(mu, coup):
            return self._dgdmu(coup, loop, mu)
        g0, mu0 = self.ginit, self.initial_mu
        dmu = mu_final - self.initial_mu
        r = ode(deriv).set_integrator('dopri5')
        r.set_initial_value(g0, mu0)
        solution = r.integrate(r.t+dmu)
        g_at_mu_final = solution

        final_dict = {}
        final_dict['g1'] = g_at_mu_final[0]
        final_dict['g2'] = g_at_mu_final[1]
        final_dict['gs'] = g_at_mu_final[2]
        final_dict['yt'] = g_at_mu_final[3]
        final_dict['yb'] = g_at_mu_final[4]
        final_dict['yc'] = g_at_mu_final[5]
        final_dict['ytau'] = g_at_mu_final[6]
        final_dict['lamH'] = g_at_mu_final[7]
        final_dict['lampH'] = g_at_mu_final[8]
        final_dict['lampHp'] = g_at_mu_final[9]
        final_dict['lambdaphi'] = np.array(g_at_mu_final[10:])
        
        return final_dict


# Now let's run the couplings!

cc = couplings(3, 0, coupj3, 91.1876)

print("The running couplings:")
print("\n")
print("The couplings at mu=1TeV:", cc.run(2, 1e3))
print("\n")
print("The couplings at mu=10TeV:", cc.run(2, 1e4))
print("\n")
print("The couplings at mu=100TeV:", cc.run(2, 1e5))

sys.exit()



