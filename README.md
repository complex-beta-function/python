**Two-loop Beta Function for Complex Scalar Electroweak Multiplets**
=====

`complex_multiplet_beta_function.py` is a simple `python3` module that provides analytic expressions for the beta functions of the standard model extended by an electroweak multiplet furnishing a general, irreducible representation of the electroweak gauge group SU(2) x U(1), up to the two-loop level.

See [this arxiv preprint](https://arxiv.org/abs/2007.13755) for the details.

## Installation and prerequisites

`complex_multiplet_beta_function.py` needs `python3` and `numpy`, as well as the `pywigxjpf` package that can be obtained [here](http://fy.chalmers.se/subatom/wigxjpf/).

## Usage

`complex_multiplet_beta_function.py` provides functions that return the numerical value of the beta function, in dependence on the weak isospin `jphi`, the hypercharge `Yphi`, at one-loop and two-loop order.

The couplings have to be given as a python dictionary.

Here is a simple example on how to use the module.

Import the package:
```
import complex_multiplet_beta_function as cmb
```

Define the coupling dictionary (defined here at the renormalization scale of the Z-boson mass, for `jphi=3`, so that we have four independent scalar self interactions):
```
coup = {'g1': 0.357, 'g2': 0.652, 'gs': 1.163, 'yt': 0.932, 'yb': 0.0164, 'yc': 0.00361, 'ytau': 0.0102,\
        'lamH': 0.504, 'lampH': 0.5, 'lampHp': 0.5, 'lambdaphi': np.array([0.5, 0.5, 0.5, 0.5])}
```

Calculate the value of `beta_g2` at one-loop accuracy:
```
print(cmb.beta_g2(3, 1, coup, 1))
```

Calculate the value of `beta_lamdaphi` at two-loop accuracy, with hypercharge `Yphi=1`:
```
print(cmb.beta_lambdaphi(3, 1, coup, 2))
```

The included `example.py` file has a few more basic examples for using the functions provided by the code, including a simple routine to calculate the RG evolution of the couplings.

## Citation
If you use our results, please cite us! To get the `BibTeX` entries, click on: [inspirehep query](https://inspirehep.net/search?p=arxiv:2007.13755) 


